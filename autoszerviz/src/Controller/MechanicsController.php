<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Mechanics Controller
 *
 * @property \App\Model\Table\MechanicsTable $Mechanics
 *
 * @method \App\Model\Entity\Mechanic[] paginate($object = null, array $settings = [])
 */
class MechanicsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $mechanics = $this->paginate($this->Mechanics);

        $this->set(compact('mechanics'));
        $this->set('_serialize', ['mechanics']);
    }

    /**
     * View method
     *
     * @param string|null $id Mechanic id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $mechanic = $this->Mechanics->get($id, [
            'contain' => ['Appointments']
        ]);

        $this->set('mechanic', $mechanic);
        $this->set('_serialize', ['mechanic']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $mechanic = $this->Mechanics->newEntity();
        if ($this->request->is('post')) {
            $mechanic = $this->Mechanics->patchEntity($mechanic, $this->request->getData());
            if ($this->Mechanics->save($mechanic)) {
                $this->Flash->success(__('The mechanic has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The mechanic could not be saved. Please, try again.'));
        }
        $this->set(compact('mechanic'));
        $this->set('_serialize', ['mechanic']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Mechanic id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $mechanic = $this->Mechanics->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $mechanic = $this->Mechanics->patchEntity($mechanic, $this->request->getData());
            if ($this->Mechanics->save($mechanic)) {
                $this->Flash->success(__('The mechanic has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The mechanic could not be saved. Please, try again.'));
        }
        $this->set(compact('mechanic'));
        $this->set('_serialize', ['mechanic']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Mechanic id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $mechanic = $this->Mechanics->get($id);
        if ($this->Mechanics->delete($mechanic)) {
            $this->Flash->success(__('The mechanic has been deleted.'));
        } else {
            $this->Flash->error(__('The mechanic could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
