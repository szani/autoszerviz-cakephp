<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
  <?= $this->Element('oldalsoMenu', array('tipus'=>'Appointment', 'tipusok'=>'Appointments')); ?>
</nav>
<div class="appointments view large-9 medium-8 columns content">
    <h3><?= h($appointment->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Customer') ?></th>
            <td><?= $appointment->has('customer') ? $this->Html->link($appointment->customer->name, ['controller' => 'Customers', 'action' => 'view', $appointment->customer->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Mechanic') ?></th>
            <td><?= $appointment->has('mechanic') ? $this->Html->link($appointment->mechanic->name, ['controller' => 'Mechanics', 'action' => 'view', $appointment->mechanic->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Date') ?></th>
            <td><?= h($appointment->date->i18nFormat('yyyy-MM-dd HH:mm')) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($appointment->created->i18nFormat('yyyy-MM-dd HH:mm')) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($appointment->modified->i18nFormat('yyyy-MM-dd HH:mm')) ?></td>
        </tr>
    </table>
</div>
