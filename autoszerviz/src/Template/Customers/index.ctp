<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
  <?= $this->Element('oldalsoMenu', array('tipus'=>'Customer', 'tipusok'=>'Customers')); ?>
</nav>
<div class="customers index large-9 medium-8 columns content">
    <h3><?= __('Customers') ?></h3>
    <div class="table-scroll">
      <table cellpadding="0" cellspacing="0" style="width:auto; min-width:100%">
          <thead>
              <tr>
                  <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                  <th scope="col" width="150"><?= $this->Paginator->sort('email') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('phone') ?></th>
                  <th scope="col" width="50"><?= $this->Paginator->sort('zip') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('city') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('street') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('insurance_id') ?></th>
                  <th scope="col" class="actions"><?= __('Actions') ?></th>
              </tr>
          </thead>
          <tbody>
              <?php foreach ($customers as $customer): ?>
              <tr>
                  <td><?= h($customer->name) ?></td>
                  <td><?= h($customer->email) ?></td>
                  <td><?= h($customer->phone) ?></td>
                  <td><?= h($customer->zip) ?></td>
                  <td><?= h($customer->city) ?></td>
                  <td><?= h($customer->street) ?></td>
                  <td><?= $customer->has('insurance') ? $this->Html->link($customer->insurance->name, ['controller' => 'Insurances', 'action' => 'view', $customer->insurance->id]) : '' ?></td>
                  <td class="actions">
                      <?= $this->Html->link(__('View'), ['action' => 'view', $customer->id]) ?>
                      <?= $this->Html->link(__('Edit'), ['action' => 'edit', $customer->id]) ?>
                      <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $customer->id], ['confirm' => __('Are you sure you want to delete # {0}?', $customer->id)]) ?>
                  </td>
              </tr>
              <?php endforeach; ?>
          </tbody>
      </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
