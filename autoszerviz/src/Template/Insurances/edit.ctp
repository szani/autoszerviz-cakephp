<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
  <?= $this->Element('oldalsoMenu', array('tipus'=>'Insurance', 'tipusok'=>'Insurances')); ?>
</nav>
<div class="insurances form large-9 medium-8 columns content">
    <?= $this->Form->create($insurance) ?>
    <fieldset>
        <legend><?= __('Edit Insurance') ?></legend>
        <?php
            echo $this->Form->control('name');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
