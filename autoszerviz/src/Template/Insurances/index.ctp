<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
  <?= $this->Element('oldalsoMenu', array('tipus'=>'Insurance', 'tipusok'=>'Insurances')); ?>
</nav>
<div class="insurances index large-9 medium-8 columns content">
    <h3><?= __('Insurances') ?></h3>
    <table cellpadding="0" cellspacing="0" style="width:auto; min-width:100%">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($insurances as $insurance): ?>
            <tr>
                <td><?= h($insurance->name) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $insurance->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $insurance->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $insurance->id], ['confirm' => __('Are you sure you want to delete # {0}?', $insurance->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
