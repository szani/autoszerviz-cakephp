<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
  <?= $this->Element('oldalsoMenu', array('tipus'=>'Mechanic', 'tipusok'=>'Mechanics')); ?>
</nav>
<div class="mechanics form large-9 medium-8 columns content">
    <?= $this->Form->create($mechanic) ?>
    <fieldset>
        <legend><?= __('Add Mechanic') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('on_vacation');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
