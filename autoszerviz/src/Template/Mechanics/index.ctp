<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
  <?= $this->Element('oldalsoMenu', array('tipus'=>'Mechanic', 'tipusok'=>'Mechanics')); ?>
</nav>
<div class="mechanics index large-9 medium-8 columns content">
    <h3><?= __('Mechanics') ?></h3>
    <table cellpadding="0" cellspacing="0" style="width:auto; min-width:100%">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('on_vacation') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($mechanics as $mechanic): ?>
            <tr>
                <td><?= h($mechanic->name) ?></td>
                <td><?= $mechanic->on_vacation ? __('Yes') : __('No'); ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $mechanic->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $mechanic->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $mechanic->id], ['confirm' => __('Are you sure you want to delete # {0}?', $mechanic->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
