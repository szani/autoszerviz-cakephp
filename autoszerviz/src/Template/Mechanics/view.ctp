<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
  <?= $this->Element('oldalsoMenu', array('tipus'=>'Mechanic', 'tipusok'=>'Mechanics')); ?>
</nav>
<div class="mechanics view large-9 medium-8 columns content">
    <h3><?= h($mechanic->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($mechanic->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($mechanic->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($mechanic->created->i18nFormat('yyyy-MM-dd HH:mm')) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($mechanic->modified->i18nFormat('yyyy-MM-dd HH:mm')) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('On Vacation') ?></th>
            <td><?= $mechanic->on_vacation ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Appointments') ?></h4>
        <?php if (!empty($mechanic->appointments)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Customer Id') ?></th>
                <th scope="col"><?= __('Date') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($mechanic->appointments as $appointments): ?>
            <tr>
                <td><?= $this->Html->link($this->Format->getName($appointments->customer_id, 'customers'), ['controller' => 'Customers', 'action' => 'view', $appointments->customer_id]) ?></td>
                <td><?= h($appointments->date->i18nFormat('yyyy-MM-dd HH:mm')) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Appointments', 'action' => 'view', $appointments->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Appointments', 'action' => 'edit', $appointments->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Appointments', 'action' => 'delete', $appointments->id], ['confirm' => __('Are you sure you want to delete # {0}?', $appointments->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
